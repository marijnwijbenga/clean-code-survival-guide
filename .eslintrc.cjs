module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/typescript/recommended"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    // override/add rules settings here, such as:
    // '@typescript-eslint/no-var-requires': 0
    "quotes": ["error", "double"]
  }
};